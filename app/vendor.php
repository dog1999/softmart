<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class vendor extends Authenticatable 
{
    protected $guard = 'vendors';
    protected $table = 'vendors';
    protected $fillable = ['username','password','email','phone','address','name_store','code_active'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
