<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class vendorRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'namestore' => 'min:3|max:100',
            'username' => 'min:3|max:12',
            'password' => 'min:6|max:20}comfirmed',
            'repassword' => 'same:password',
            'email' => 'email|min:10|max:100',
            'phone' => 'numeric',
            'address' => 'min:5|max:500'
        ];
    }

    public function messages(){
        return [
            'namestore.min' => 'Tên doanh nghiệp phải có ít nhất 3 ký tự',
            'namestore.max' => 'Tên doanh nghiệp không được vượt quá 100 ký tự',
            'username.min' => 'Tên đăng nhập phải có ít nhất 3 ký tự',
            'username.max' => 'Tên đăng nhập không được vượt quá 12 ký tự',
            'password.min' => 'Mật khẩu không được ít hơn 6 ký tự',
            'password.max' => 'Mật khẩu không được vượt quá 20 ký tự',
            'repassword.same' => 'Mật khẩu xác nhận không khớp',
            'email.min' => 'Email không được ít hơn 10 ký tự',
            'email.max' => 'Email không được ít hơn 100 ký tự',
            'phone.numeric' => 'Số điện thoại không đúng định dạng',
            'address.min' => 'Địa chỉ không được ít hơn 5 ký tự',
            'address.max' => 'Địa chỉ không được nhiều hơn 500'
        ];
    }
}
