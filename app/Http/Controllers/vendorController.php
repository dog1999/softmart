<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\vendorRegister;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\vendor;
use Illuminate\Support\Str;
use Mail;
use Illuminate\Support\Facades\URL;

class vendorController extends Controller
{
    public function register(){
        return view('fontend.vendor.register');
    }

    public function postRegister(vendorRegister $request){
        //kiểm tra xem có bị trùng username hay không 
        $check_unique = vendor::where('username',$request->username)->count();
        if($check_unique == 0){
            $code = Str::random();
            $link = route('vendor.active',['username'=>$request->username,'code'=>$code]);
            $vendor = vendor::create([
                'name_store' => $request->input('namestore'),
                'username' => $request->input('username'),
                'password' => bcrypt($request->input('pasword')),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'address' => $request->input('address'),
                'code_active' => $code,
                'slug' => Str::slug($request->input('namestore'),'-')
            ]);

            $content = 'Tài khoản của bản đã đăng ký thành công , hãy click vào link dưới đây để kích hoạt tài khoản ' . $link;
            Mail::raw($content, function ($message) use($request) {
                $message->from('sunshineweb.vn@gmail.com', 'SoftMart');
                $message->to($request->email,$request->email);
                $message->subject('Kích hoạt tài khoản');
            });

            return redirect()->back()->withInput($request->input())->with('thongbao','Bạn đã đăng ký tài khoản thành công, để kích hoạt tài khoản hãy vào mail và làm theo hướng dẫn!');
        }else{
            return redirect()->back()->withInput($request->input())->with('thongbao','Tên đăng nhập đã được sử dụng');
        }
    }

    public function active(){
        if(isset($_GET['username']) && !empty($_GET['username']) && isset($_GET['code']) && !empty($_GET['code'])){
            $username = $_GET['username'];
            $code = $_GET['code'];
            $vendor = vendor::where('username',$username)->where('code_active',$code)->count();
            if($vendor == 1){
                vendor::where('username',$username)->update(['is_active'=>1]);
            }
            echo "<script>alert('Kích hoạt tài khoản thành công !');window.location.href='".URL::route('vendor.login')."'</script>";
        }else{
            return redirect()->back();
        }
    }

    public function getlogin(){
        return view('fontend.vendor.login');
    }

    public function login(Request $request){
        if (Auth::guard('vendors')->attempt(['username' => $request->username, 'password' => $request->password])) {
            $check_active = vendor::where('username',$request->username)->where('is_active',1)->count();
            if($check_active > 0){
                return redirect()->route('admin');
            }else{
                session()->flush();
                return redirect()->back()->with('thongbao',"Tài khoản của bạn chưa được kích hoạt");
            }
        }else{
            return redirect()->back()->with('thongbao','Mật khẩu hoặc tài khoản không chính xác');
        }
    }
}