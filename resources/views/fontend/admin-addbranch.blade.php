@extends('fontend.layout.admin')
@section('content')
<style>
    .span9 form input{
        width: 850px;
    }
    .span9 form input.add{
        display: block;
        width: 100px;
        margin-left: 350px !important;
        margin-top: 30px !important;
        background-color: blue;
    }
</style>
	<div class="span9">
        <form style="margin-left: 50px" action="" method="POST">
            @csrf
            <input style="display:none" type="text" name="id" value="">
            <label for="">Name Branch</label>
            <input type="text" name="name" value="">

            <label for="">Hotline</label>
            <input type="text" name="hotline" value="">

            <label for="">Email</label>
            <input type="text" name="email" value="">

            <label for="">Facebook</label>
            <input type="text" name="facebook" value="">

            <label for="">Instagram</label>
            <input type="text" name="instagram" value="">

            <label for="">Twitter</label>
            <input type="text" name="twitter" value=""><br>
            
            <label for="">Address</label>
            <input type="text" name="address" value=""><br>

            <label for="">Phone</label>
            <input type="text" name="phone" value=""><br>

            <input class="add" style="margin-left:60px" type="submit" name="add" value="Add">
        </form>
    </div>
@endsection