@extends('fontend.base')
@section('content')
    @if(session('thongbao'))
        <div class = "alert alert-success">
            {{ session('thongbao') }}
        </div>
    @endif
    <div class="container mt-5 mb-5">
        @if(count($errors)>0)
            <div class = "alert alert-danger">
                @foreach($errors->all() as $err)
                    {{$err}}<br>
                @endforeach
            </div>
        @endif

        <div class="box">

            <h1>Đăng ký</h1>
            <form action="{{route('vendor.postregister')}}" method="post">
                @csrf
                <div class="input-box">
                    <input type="text" name="namestore" value="{{ old('namestore') }}" required />
                    <label for="">Tên doanh nghiệp</label>
                </div>
                <div class="input-box">
                    <input type="text" name="username" value="{{ old('username') }}"" required />
                    <label for="">Tên đăng nhập</label>
                </div>
                <div class="input-box">
                    <input type="password" name="password" required />
                    <label for="">Mật khẩu</label>
                </div>
                <div class="input-box">
                    <input type="password" name="repassword" required />
                    <label for="">Nhập lại mật khẩu</label>
                </div>
                <div class="input-box">
                    <input type="text" name="email" value="{{ old('email') }}" required />
                    <label for="">Email</label>
                </div>
                <div class="input-box">
                    <input type="text" name="phone" value="{{ old('phone') }}" required />
                    <label for="">Số điện thoại</label>
                </div>
                <div class="input-box">
                    <input type="text" name="address" value="{{ old('address') }}" required />
                    <label for="">Địa chỉ doanh nghiệp</label>
                </div>
                <input type="submit" value="Submit" />
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function openNav() {
            document.getElementById("mySidepanel").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function() {
                this.classList.toggle("active-1");
                var dropdownContent = this.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                } else {
                    dropdownContent.style.display = "block";
                }
            });
        }
    </script>
@endsection
