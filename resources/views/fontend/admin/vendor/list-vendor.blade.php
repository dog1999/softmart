@extends('fontend.layout.admin')
@section('content')
	<div class="span9">
		<div class="content">
			<div class="module message">
				<div class="module-head">
					<h3>Danh sách các đối tác</h3>
				</div>
				<div class="module-option clearfix">
					<div class="pull-right">
						<form action="{{ route('adminsearchuser') }}" method="get">
							<input type="text" name="q" id="">
							<button style="margin-bottom: 10px;" type="submit"><i class="fas fa-search"></i></button>
						</form>
					</div>
				</div>
				<div>
					<form style="margin-bottom: 10px;margin-left: 5px" action="{{ route('exportlistuser') }}">
						@csrf
						<button style="background-color: aqua" value="export">Xuất Excel</button>
					</form>
				</div>
				<div class="module-body table">
					<table class="table table-message">
						<thead>
							<tr class="heading">
								<td class="cell-author hidden-phone hidden-tablet">
									Tên doanh nghiệp
								</td>
								<td class="cell-title">
									username
								</td>
								<td class="cell-time align-right">
									Ngày tạo
                                </td>
                                <td class="cell-time align-right">
									Ngày kích hoạt tài khoản
								</td>
								<td class="cell-time align-right">
								</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($data as $user)
								<tr class="read">
									<td class="cell-author hidden-phone hidden-tablet">
										<a href="{{ route('vendor.profile', ['slug'=>$user->slug]) }}">{{$user->name_store}}</a>
									</td>
									<td class="cell-title">
										{{$user->username}}
									</td>
									<td class="cell-time align-right">
										{{date_format($user->created_at,'Y-m-d')}}
									</td>
									<td class="cell-time align-right">
                                        @if (isset($user->updated_at))
                                            {{date_format($user->updated_at,'Y-m-d')}}
                                        @endif
									</td>
									<?php
										$checkdelete = 0;
										foreach(Auth::user()->permission as $permission){
											if($permission->slug_name == 'xoa-nguoi-dung'){
												$checkdelete = 1;
											}
										}
									?>
									<td>
										@if ($checkdelete == 1)
											<i onclick="var result = confirm('Bạn có thực sự muốn xóa người dùng này?')
											if(result == true){
												window.location.href = '{{URL::to('admin/deleteuser?id='.$user->id)}}'
											}else{
											}" class="fas fa-user-times"></i>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="module-foot">
				</div>
			</div>
		</div>
		<!--/.content-->
	</div>
	<!--/.span9-->
@endsection
