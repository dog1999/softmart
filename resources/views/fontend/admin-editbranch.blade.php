@extends('fontend.layout.admin')
@section('content')
<style>
    .span9 form input{
        width: 850px;
    }
    .span9 form input.add{
        display: block;
        width: 100px;
        margin-left: 350px !important;
        margin-top: 30px !important;
        background-color: blue;
    }
</style>
	<div class="span9">
        <form style="margin-left: 50px" action="" method="POST">
            @csrf
            <input style="display:none" type="text" name="id" value="{{ $data[0]->id }}">
            <label for="">Name Branch</label>
            <input type="text" name="name" value="{{ $data[0]->name_branch }}">

            <label for="">Hotline</label>
            <input type="text" name="hotline" value="{{ $data[0]->hotline }}">

            <label for="">Email</label>
            <input type="text" name="email" value="{{ $data[0]->email }}">

            <label for="">Facebook</label>
            <input type="text" name="facebook" value="{{ $data[0]->facebook }}">

            <label for="">Instagram</label>
            <input type="text" name="instagram" value="{{ $data[0]->instagram }}">

            <label for="">Twitter</label>
            <input type="text" name="twitter" value="{{ $data[0]->twitter }}"><br>
            
            <label for="">Address</label>
            <input type="text" name="address" value="{{ $data[0]->address }}"><br>

            <label for="">Phone</label>
            <input type="text" name="phone" value="{{ $data[0]->phone }}"><br>

            @if ($data[0]->id == 1)
                <label for="">About</label>
                <input type="text" name="about" value="{{ $data[0]->about }}"><br>

                <label for="">Account number</label>
                <input type="text" name="account_number" value="{{ $data[0]->account_number }}"><br>

                <label for="">Bank name</label>
                <input type="text" name="bank_name" value="{{ $data[0]->bank_name }}"><br>

                <label for="">Company name</label>
                <input type="text" name="company_name" value="{{ $data[0]->company_name }}"><br>
            @endif

            <input class="add" style="margin-left:60px" type="submit" name="update" value="Update">
        </form>
    </div>
@endsection